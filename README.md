# CHIP8
The code behind Ganden, Noll, Wyatt, Jake, and Jacob's bot.

## License
This project is licensed under the GNU GPLv3 license. See [LICENSE](LICENSE).
