#pragma config(I2C_Usage, I2C1, i2cSensors)
#pragma config(Sensor, dgtl1,  armUp,          sensorTouch)
#pragma config(Sensor, dgtl2,  sonar,          sensorSONAR_inch)
#pragma config(Sensor, I2C_1,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign )
#pragma config(Sensor, I2C_2,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign )
#pragma config(Sensor, I2C_3,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign )
#pragma config(Sensor, I2C_4,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign )
#pragma config(Sensor, I2C_5,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign )
#pragma config(Sensor, I2C_6,  ,               sensorQuadEncoderOnI2CPort,    , AutoAssign )
#pragma config(Motor,  port1,           rF,            tmotorVex393_HBridge, openLoop, reversed, encoderPort, I2C_2)
#pragma config(Motor,  port2,           rArmU,         tmotorVex393HighSpeed_MC29, openLoop, reversed, encoderPort, I2C_6)
#pragma config(Motor,  port3,           lArmU,         tmotorVex393HighSpeed_MC29, openLoop, encoderPort, I2C_5)
#pragma config(Motor,  port6,           rArmD,         tmotorVex393HighSpeed_MC29, openLoop, reversed)
#pragma config(Motor,  port7,           lArmD,         tmotorVex393HighSpeed_MC29, openLoop, reversed)
#pragma config(Motor,  port8,           rR,            tmotorVex393_MC29, openLoop, reversed, encoderPort, I2C_1)
#pragma config(Motor,  port9,           lR,            tmotorVex393_MC29, openLoop, encoderPort, I2C_4)
#pragma config(Motor,  port10,          lF,            tmotorVex393_HBridge, openLoop, encoderPort, I2C_3)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//

// This file is part of Vex-920-2016, and is therefore licensed under the GNU GPLv3 public license.
// Copyright (C) 2017 Ganden Schaffner

/* Controls
	Ch1: Movement: Arcade: Rotation
	Ch3: Movement: Arcade: Forwards and backwards
	Ch4: Movement: Arcade: Left and right
	Btn6U: Arm: Raise
	Btn6D: Arm: Lower
*/

#pragma platform(VEX2)
#pragma competitionControl(Competition)

// Include files
#include "Vex_Competition_Includes.c"
#include "libraries/smartMotorLib/SmartMotorLib.c"

// Defined Variables
#define M_PI acos(-1.0);

// Variables
int fullArmMovement = 0;

// Methods
/*! Initializes everything needed to use smartMotorLib.
*/
void initSmartMotors() {
	SmartMotorsInit();
	SmartMotorPtcMonitorEnable();
	SmartMotorsAddPowerExtender(lR, rR, lArmD, rArmD);
	SmartMotorRun();
}

/*! Applies a deadband to the given input channel.
	\param channel The input channel to apply the deadband to.
*/
float deadband(float channel) {
	return (abs(channel) < 25) ? 0 : channel;
}

/*! Moves that bot based on input from the left and right sticks.
*/
void moveStickArcade() {
	SetMotor(rF, deadband(vexRT[Ch3]) - deadband(vexRT[Ch4]) - 0.5 * deadband(vexRT[Ch1]));
	SetMotor(rR, deadband(vexRT[Ch3]) + deadband(vexRT[Ch4]) - 0.5 * deadband(vexRT[Ch1]));
	SetMotor(lF, deadband(vexRT[Ch3]) + deadband(vexRT[Ch4]) + 0.5 * deadband(vexRT[Ch1]));
	SetMotor(lR, deadband(vexRT[Ch3]) - deadband(vexRT[Ch4]) + 0.5 * deadband(vexRT[Ch1]));
}

/*! Moves the bot based on input from the left and right sticks,
	using tank controls.
*/
void moveStickTank() {
	SetMotor(rF, deadband(vexRT[Ch2]) - deadband(vexRT[Ch1]));
	SetMotor(rR, deadband(vexRT[Ch2]) + deadband(vexRT[Ch1]));
	SetMotor(lF, deadband(vexRT[Ch3]) + deadband(vexRT[Ch4]));
	SetMotor(lR, deadband(vexRT[Ch3]) - deadband(vexRT[Ch4]));
}

/*! Moves the bot in a given direction at a given speed for a given duration.
	\param ms The number of ms to drive forward for.
	\param direction The direction to move in, given in standard cartesian degrees.
		Must be a float.
	\param speed The speed to drive the bot at (Can be negative to drive in
		the opposite direction).
*/
void moveDirectionTime(int ms, float direction, int speed) {
	for(int i = 0; i <= ms; i += 20) {
		SetMotor(rF, (int)((speed * sinDegrees(direction)) - (speed * cosDegrees(direction))));
		SetMotor(rR, (int)((speed * sinDegrees(direction)) + (speed * cosDegrees(direction))));
		SetMotor(lF, (int)((speed * sinDegrees(direction)) + (speed * cosDegrees(direction))));
		SetMotor(lR, (int)((speed * sinDegrees(direction)) - (speed * cosDegrees(direction))));
		wait1Msec(20);
	}
	SetMotor(rF, 0);
	SetMotor(rR, 0);
	SetMotor(lF, 0);
	SetMotor(lR, 0);
}

/*! Moves the bot in a given direction at a given speed for a given distance.
	\param distance The distance, in inches, to travel.
	\param direction The direction to move in, given in standard cartesian degrees.
		Must be a float.
	\param speed The speed to drive the bot at.
*/
void moveDirectionDistance(float distance, float direction, int speed) {
	nMotorEncoder[rF] = 0;
	nMotorEncoder[rR] = 0;
	nMotorEncoder[lF] = 0;
	nMotorEncoder[lR] = 0;
	// Torque-geared 393 motor IEMs measure 627.2 counts per revolution.
	// Radius of large omni wheels is 2in.
	float circumference = 4 * M_PI;
	float distanceInRevs = distance / circumference;
	int intendedRF = sinDegrees(direction - 45.0) * distanceInRevs * 627.2;
	int intendedLF = cosDegrees(direction - 45.0) * distanceInRevs * 627.2;
	int intendedLR = sinDegrees(direction - 45.0) * distanceInRevs * 627.2;
	int intendedRR = cosDegrees(direction - 45.0) * distanceInRevs * 627.2;
	while(	abs(nMotorEncoder[rF]) < abs(intendedRF) &&
			abs(nMotorEncoder[rR]) < abs(intendedRR) &&
			abs(nMotorEncoder[lF]) < abs(intendedLF) &&
			abs(nMotorEncoder[lR]) < abs(intendedLR)) {
		SetMotor(rF, (int)((speed * sinDegrees(direction)) - (speed * cosDegrees(direction))));
		SetMotor(rR, (int)((speed * sinDegrees(direction)) + (speed * cosDegrees(direction))));
		SetMotor(lF, (int)((speed * sinDegrees(direction)) + (speed * cosDegrees(direction))));
		SetMotor(lR, (int)((speed * sinDegrees(direction)) - (speed * cosDegrees(direction))));
		wait1Msec(20);
	}
	SetMotor(rF, 0);
	SetMotor(rR, 0);
	SetMotor(lF, 0);
	SetMotor(lR, 0);
}

/*! Raises and lowers the arm based on input from Btn6U and Btn6D.
*/
void armControl() {
	int direction = vexRT[Btn6U] ? 1 : vexRT[Btn6D] ? -1 : 0;
	if(!SensorValue[armUp] || (SensorValue[armUp] && direction == -1)) {
		SetMotor(rArmU, direction * 127);
		SetMotor(rArmD, direction * 127);
		SetMotor(lArmU, direction * 127);
		SetMotor(lArmD, direction * 127);
	} else {
		SetMotor(rArmD, 0);
		SetMotor(lArmU, 0);
		SetMotor(lArmD, 0);
		SetMotor(rArmU, 0);
	}
}

/*! Raises or lowers the arm at a certain speed for a certain duration.
	\param ms The number of ms to raise/lower the arm for.
	\param speed The speed to raise/lower the arm at (Can be negative to lower the arm).
*/
void liftArmTime(int ms, int speed) {
	for(int i = 0; i <= ms; i += 20) {
		SetMotor(rArmU, speed);
		SetMotor(rArmD, speed);
		SetMotor(lArmD, speed);
		SetMotor(lArmU, speed);
		wait1Msec(20);
	}
	SetMotor(rArmU, 0);
	SetMotor(rArmD, 0);
	SetMotor(lArmU, 0);
	SetMotor(lArmD, 0);
}

/*! Fully raises or lowers the arm. It the intent is to lower the arm,
	fullArmMovement must be calibrated. fullArmMovement will be calibrated
	by this method on the first lift.
	\param speed The speed to raise/lower the arm at.
	\param direction The direction to move the arm, either +1 (up) or -1 (down).
*/
void toggleArm(int speed, int direction) {
	if(fullArmMovement == 0) {
		nMotorEncoder[rArmU] = 0;
		nMotorEncoder[lArmU] = 0;
		while(!SensorValue[armUp]) {
			SetMotor(rArmU, speed);
			SetMotor(rArmD, speed);
			SetMotor(lArmD, speed);
			SetMotor(lArmU, speed);
		}
		SetMotor(rArmU, 0);
		SetMotor(rArmD, 0);
		SetMotor(lArmU, 0);
		SetMotor(lArmD, 0);
		fullArmMovement = nMotorEncoder[rArmU];
	} else {
		// Do not allow the arm to move upward if it is already fully up
		if(direction == 1) {
			while(!SensorValue[armUp] && (nMotorEncoder[rArmU] < fullArmMovement - 30)) {
				SetMotor(rArmU, speed * direction);
				SetMotor(rArmD, speed * direction);
				SetMotor(lArmD, speed * direction);
				SetMotor(lArmU, speed * direction);
				wait1Msec(20);
			}
		} else if(direction == -1) {
			while(nMotorEncoder[rArmU] > 30) {
				SetMotor(rArmU, speed * direction);
				SetMotor(rArmD, speed * direction);
				SetMotor(lArmD, speed * direction);
				SetMotor(lArmU, speed * direction);
				wait1Msec(20);
			}
		}

		// Make sure that it gets all the way down
		clearTimer(T4);
		while(time1[T4] < 100) {
			SetMotor(rArmU, speed * direction);
			SetMotor(rArmD, speed * direction);
			SetMotor(lArmD, speed * direction);
			SetMotor(lArmU, speed * direction);
			wait1Msec(20);
		}
	}
	SetMotor(rArmU, 0);
	SetMotor(rArmD, 0);
	SetMotor(lArmU, 0);
	SetMotor(lArmD, 0);
}

void pre_auton() {
	// Set bStopTasksBetweenModes to false if you want to keep user created tasks
	// running between Autonomous and Driver controlled modes. You will need to
	// manage all user created tasks if set to false.
	bStopTasksBetweenModes = true;

	// Set bDisplayCompetitionStatusOnLcd to false if you don't want the LCD
	// used by the competition include file, for example, you might want
	// to display your team name on the LCD in this function.
	bDisplayCompetitionStatusOnLcd = false;

	// All activities that occur before the competition starts
	// Example: clearing encoders, setting servo positions, ...
}

task autonomous() {
	stopTask(usercontrol);
	initSmartMotors();
	moveDirectionDistance(45.0, 90.0, 127);
	toggleArm(127, 1);
}

task usercontrol() {
	stopTask(autonomous);
	initSmartMotors();
	while (true) {
		clearLCDLine(0);
		clearLCDLine(1);
		displayLCDCenteredString(0, "Sonar: ");
		displayNextLCDNumber(SensorValue[sonar]);
		moveStickArcade();
		armControl();
		wait1Msec(20);
	}
}
